package com.uz.chat.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.internal.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private MessageType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    private Users user;

    @Column(name = "users_id")
    private Long user_id;

    @ManyToOne
    @JoinColumn(name = "chat_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    private Chat chat;

    private String message;
//    private String content;
    private String sender;

    @NotNull
    @DateTimeFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss.SSS")
    private Date crated_at;

    public enum MessageType {
        CHAT,
        JOIN,
        LEAVE
    }
}
