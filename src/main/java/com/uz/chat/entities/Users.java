package com.uz.chat.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.NotNull;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class Users  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotNull
    private String name;

    @DateTimeFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss.SSS")
    private Date created_at;

    private String firstName;

    private String surname;

    @OneToMany(mappedBy="owner")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    private List<Chat> chats = new ArrayList<Chat>();

    private String password;

    private boolean deleted; //true bolsa ochirilgan boladi
}
