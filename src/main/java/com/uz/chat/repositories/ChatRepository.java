package com.uz.chat.repositories;

import com.uz.chat.entities.Chat;
import org.springframework.data.jpa.repository.JpaRepository;



public interface ChatRepository extends JpaRepository<Chat, Integer> {
    Chat findById(Long id);
}
