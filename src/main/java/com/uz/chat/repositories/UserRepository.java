package com.uz.chat.repositories;

import com.uz.chat.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UserRepository extends JpaRepository<Users, Integer> {
    Users findByName(String name);


    Users findById(Long id);

    List<Users> findAllById(Long id);

    List<Users> findAllByChats(Users us);
}
