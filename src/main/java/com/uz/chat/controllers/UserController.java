package com.uz.chat.controllers;

import com.uz.chat.dtos.LoginDto;
import com.uz.chat.entities.Users;
import com.uz.chat.services.interfaces.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;

    public UserController(UserService userService, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
    }

//    @PostMapping("/registr/{name}")
//    public ResponseEntity<Users> createUser(@PathVariable(value = "name")String name){
//        return ResponseEntity.ok(userService.save(name));
//    }

    @PostMapping("/registrations")
    private ResponseEntity<Users> create(@RequestBody Users users){
        return ResponseEntity.ok(userService.save(users));
    }

    @GetMapping("/allUsers")
    public ResponseEntity<List<Users>> getAllUsers(){
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("/getAllByChat")
    public ResponseEntity<List<Users>> getAllByChat(@Valid Users users){
        return ResponseEntity.ok(userService.findAllByChats(users));
    }

//    @PostMapping("/login")
//    public ResponseEntity<String> authenticateUser(@RequestBody LoginDto loginDto){
//        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
//                loginDto.getName(), loginDto.getPassword()));
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        return new ResponseEntity<>("User signed-in successfully!.", HttpStatus.OK);
//    }
}
