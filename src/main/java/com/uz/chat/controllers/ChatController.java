package com.uz.chat.controllers;

import com.uz.chat.entities.Chat;
import com.uz.chat.entities.Users;
import com.uz.chat.services.ChatService;
import com.uz.chat.services.interfaces.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/chat/")
@CrossOrigin
public class ChatController {
    private final ChatService service;

    public final UserService userService;

    public ChatController(ChatService service, UserService userService) {
        this.service = service;
        this.userService = userService;
    }

    @PostMapping("/create")
    public ResponseEntity<Chat> createChat(@RequestBody Chat chat){
        return ResponseEntity.ok(service.createChat(chat));
    }

    @GetMapping("/all")
    public ResponseEntity<List<Chat>> getAll(){
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping("/addUsers/{name}")
    public ResponseEntity addUsers(@PathVariable(name = "name")String name){
        Users users = userService.findByName(name);
        if (users==null){
            return ResponseEntity.badRequest().body("object null");
        }
        Chat chat = service.addUsers(users);
        return ResponseEntity.ok(chat);
    }


}
