package com.uz.chat.controllers;

import com.uz.chat.entities.Message;
import com.uz.chat.entities.Users;
import com.uz.chat.security.JwtTokenProvider;
import com.uz.chat.services.MessageService;
import com.uz.chat.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.HttpHeaders;

@RestController
@RequestMapping("/api/message/")
@CrossOrigin
public class MessageController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    private final MessageService messageService;
    private final UserServiceImpl userService;
    private final JwtTokenProvider provider;

    public MessageController(MessageService messageService, UserServiceImpl userService, JwtTokenProvider provider) {
        this.messageService = messageService;
        this.userService = userService;
        this.provider = provider;
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public Message sendMessage(@Payload Message chatMessage) {
        Message message = messageService.sendMessage(chatMessage);
        return message;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public Message addUser(@Payload Message chatMessage,
                           SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getUser().getName());
        return chatMessage;
    }
}
