package com.uz.chat.controllers;

import com.uz.chat.dtos.LoginDto;
import com.uz.chat.entities.Users;
import com.uz.chat.repositories.UserRepository;
import com.uz.chat.security.JwtTokenProvider;
import ognl.IntHashMap;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class JwtController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider provider;
    private final UserRepository repository;

    public JwtController(AuthenticationManager authenticationManager, JwtTokenProvider provider, UserRepository repository) {
        this.authenticationManager = authenticationManager;
        this.provider = provider;
        this.repository = repository;
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginDto dto) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(dto.getName(), dto.getPassword()));
        Users user = repository.findByName(dto.getName());
        if (user == null) {
            throw new UsernameNotFoundException("This user not found!");
        }
        String token = provider.createToken(user.getName());
        Map<Object, Object> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("token", token);
        return ResponseEntity.ok(map);
    }
}
