package com.uz.chat.dtos;

import lombok.Data;

@Data
public class LoginDto {
    private String name;
    private String password;
}
