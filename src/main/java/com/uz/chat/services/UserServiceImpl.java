package com.uz.chat.services;

import com.uz.chat.entities.Users;
import com.uz.chat.repositories.UserRepository;
import com.uz.chat.services.interfaces.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Users save(String name) {
        Users user1 = new Users();
        user1.setCreated_at(new Date());
        user1.setName(name);
        return repository.save(user1);
    }

    @Override
    public Users delete(Users users) {
        users.setDeleted(true);
        return repository.save(users);
    }

    @Override
    public List<Users> getAllUsers() {
        return repository.findAll();
    }

    @Override
    public Optional<Users> getById(Long id) {
        return Optional.empty();
    }

    @Override
    public Users findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Users> findAllById(Long id) {
        return repository.findAllById(id);
    }

    @Override
    public Users updateById(int id) {
        return null;
    }

//    @Override
//    public Users findByUsername(String username) {
//        return repository.findByName(username);
//    }

    @Override
    public List<Users> findAllByChats(Users usr){
        return repository.findAllByChats(usr);
    }

    @Override
    public Users findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Users save(Users users) {
        users.setCreated_at(new Date());
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        return repository.save(users);
    }

    public Users findByUsername(String username) {
        return repository.findByName(username);
    }

}
