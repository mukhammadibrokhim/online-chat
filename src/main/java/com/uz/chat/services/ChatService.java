package com.uz.chat.services;

import com.uz.chat.entities.Chat;
import com.uz.chat.entities.Users;
import com.uz.chat.repositories.ChatRepository;
import com.uz.chat.services.interfaces.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ChatService {

    private final ChatRepository repository;
    private final UserService userService;

    public ChatService(ChatRepository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    public Chat createChat(Chat chat){
        chat.setCrated_at(new Date());
        return repository.save(chat);
    }

    public List<Chat> findAll(){
        return repository.findAll();
    }

    public Chat addUsers(Users users) {
        Chat chat = repository.findById(users.getId());
        List list = new ArrayList<>();
        list.add(users);
        chat.setUsers(list);
        repository.save(chat);
        return chat;
    }
}
