package com.uz.chat.services.interfaces;

import com.uz.chat.entities.Users;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Users save(String name);

    Users delete(Users users);

    List<Users> getAllUsers();

    Optional<Users> getById(Long id);

    Users findById(Long id);

    List<Users> findAllById(Long id);

    Users updateById(int id);

    List<Users> findAllByChats(Users users);

    Users findByName(String name);

    Users save(Users users);
}
