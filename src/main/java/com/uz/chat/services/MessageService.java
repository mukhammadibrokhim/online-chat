package com.uz.chat.services;

import com.uz.chat.entities.Message;
import com.uz.chat.repositories.MessageRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MessageService {
    private final MessageRepository repository;

    public MessageService(MessageRepository repository) {
        this.repository = repository;
    }

    public Message save(Message message) {
        return repository.save(message);
    }

    public List<Message> allMessages() {
        return repository.findAll();
    }

    public Message sendMessage(Message message){
        message.setMessage(message.getMessage());
//        message.setUser(users);
//        message.setChat();
        message.setCrated_at(new Date());
        return repository.save(message);
    }

}